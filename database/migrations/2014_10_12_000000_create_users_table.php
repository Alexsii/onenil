<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('image')->default('default_image_user.png');
            $table->string('email')->nullable();
            $table->string('money')->nullable();
            $table->string('refer_link')->nullable();
            $table->string('paypal_link')->nullable();
            $table->string('link_friends')->nullable();
            $table->string('status')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }
/*'name', 'email', 'password', 'last_name', 'refer_link', 'paypal_link', 'link_friends', 'status'*/
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
