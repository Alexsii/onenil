<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
//
            $table->increments('id');
            $table->integer('id_league')->unsigned();
            $table->string('id_match')->nullable();
            $table->integer('first_team_id')->unsigned();
            $table->integer('second_team_id')->unsigned();
        });

        Schema::table('matches', function (Blueprint $table){
            $table->foreign('id_league')->references('id')->on('leagues')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('first_team_id')->references('id')->on('soccert_teams')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('second_team_id')->references('id')->on('soccert_teams')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
