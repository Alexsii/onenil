<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'dcitizen',
            'last_name' => 'eee',
            'email' => 'dev@def.com',
            'money' => '0',
            'password' => bcrypt('1Fht2Gbb'),
            'refer_link' => '',
            'paypal_link' => '',
            'link_friends' => '0',
            'status' => 'admin',
        ]);
        User::create([
            'name' => 'wwwDD',
            'last_name' => 'eDDee',
            'email' => 'devd@def.com',
            'money' => '0',
            'password' => bcrypt('qweqwe'),
            'refer_link' => '',
            'paypal_link' => '',
            'link_friends' => '0',
            'status' => 'user',
        ]);
        /*'name', 'email', 'password', 'last_name', 'refer_link', 'paypal_link', 'link_friends', 'status'*/
    }

}
