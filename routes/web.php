<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\RedirectIfAuthenticated;

Route::get('/log_in', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/open_api', 'Api\ApiController@takeAll');
Route::get('/user/logout', 'HomeController@logout');

Route::get('/info', 'HomeController@getInfo');
Route::group(['prefix' => 'catalog'], function () {
    Route::get('/', 'HomeController@getLogIn');
    Route::get('/{slug}', 'HomeController@getLogIn');
});

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => 'guest'], function () {
    Route::get('/list', 'AdminController@home');
    Route::post('/post_data_user', 'AdminController@dropInfo');
}
);
Route::get('/', function (){
    return redirect('/log_in');
});
Auth::routes();

