let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .js('resources/assets/js/home_page.js', 'public/js')
   .js('resources/assets/js/admin.js', 'public/js')
   .js('resources/assets/js/logo.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/assets/sass/home_page.scss', 'public/css')
   .sass('resources/assets/sass/admin_style.scss', 'public/css')
   .sass('resources/assets/sass/vue_style/confirmed.scss', 'public/css')
   .sass('resources/assets/sass/vue_style/logo.scss', 'public/css')
   .sass('resources/assets/sass/vue_style/create_refer.scss', 'public/css')
   .sass('resources/assets/sass/vue_style/user_account.scss', 'public/css')
   .sass('resources/assets/sass/vue_style/news.scss', 'public/css')
   .sass('resources/assets/sass/vue_style/friend_referal.scss', 'public/css')
   .sass('resources/assets/sass/vue_style/one_pick.scss', 'public/css');
