<?php

namespace App\Http\Controllers;

use App\Models\API\League;
use App\Models\API\Match;
use App\Models\API\SoccertTeam;
use App\Models\API\Sports;
use App\Models\API\TimeZone;
use App\Models\API\TypeCalls;
use function Couchbase\defaultDecoder;
use Illuminate\Support\Facades\Session;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.home');
    }

    public function getLogIn()
    {

//        $date = (date('Y') . '/' . (date('Y') + 1));
//        $idSeasonsYear = '';
//
//        /*делаем запрос на все сезоны что бы узнать потом id этого сезона************/
//        $seasons = file_get_contents('http://data2.xscores.com/exporter/?state=clientStructure&type='
//            . TypeCalls::REQUEST_SEASONS
//            . '&s=' . Sports::SOCCER);
//        $seasons = simplexml_load_string($seasons);
//        $seasons = $seasons->List->Item;
//
//        /*узнаём id этого сезона************/
//        foreach ($seasons as $season) {
//            if ($season['name'] == $date) {
//                $idSeasonsYear = $season['id'];
//                /*ура! узнали*/
//            }
//        }
//
//
//        $league = file_get_contents(
//            'http://data2.xscores.com/exporter/?state=clientStructure&type=13&ss=' . $idSeasonsYear
//        );
//        $league = simplexml_load_string($league);
//        $leagues = $league->List->Item;
//
//        foreach ($leagues as $league) {
//            League::create(['id_league' => $league['id'], 'name' => $league['name']]);
//        }
//
//        $match = file_get_contents(
//    //            'http://data2.xscores.com/exporter/?state=clientUpdate&usr=dcitizen&pwd=1Fht2Gbb&type=6&s=1&l=42709&start=' . date('Y-m-d')
//            'http://data2.xscores.com/exporter/?state=clientUpdate&usr=dcitizen&pwd=1Fht2Gbb&type=2&s=1&start=' . date('Y-m-d')
//        );
//        Session::put('match', $match);
//
//        Session::save();
//
        $match_s = Session::get('match');
        $matchs = simplexml_load_string($match_s);

        $matchs = $matchs->Sport->Matchday;
        if (count($matchs)) {
            foreach ($matchs as $mat) {
                foreach ($mat->Match as $one_match) {
                    $json = json_encode($one_match);
                    $convert_match = json_decode($json, true);

                    if ((League::where('id_league', $convert_match['@attributes']['leagueCode'])->first() == null) && ($convert_match['@attributes']['leagueCode'] != 0)) {
                        League::create([
                            'id_league' => $convert_match['@attributes']['leagueCode'],
                            'name' => $convert_match['Information']['league'],
                        ]);
                    }
//

                    if ((Match::where('id_match', $convert_match['@attributes']['id'])->first() == null) && ($convert_match['@attributes']['leagueCode'] != 0)) {
                        $id = League::where('id_league', $convert_match['@attributes']['leagueCode'])->get();

                        SoccertTeam::all();
//                       dd($idFirstTeam = SoccerTeam::where('id_team', $convert_match['Home']['@attributes']['id'])->first());
//                        $idSecondTeam = SoccerTeam::where('id_team',1)->first();
//                        dd($idFirstTeam, '/',$idSecondTeam);
//                        Match::create([
//                            'id_league' => $id[0]->id,
//                            'id_match' => $convert_match['@attributes']['id'],
//                            'first_team_id' => 2,
//                            'second_team_id' => 3,
//                        ]);
                    }
                }
//
            }
        }
        $TimeZones = file_get_contents(
            'http://data2.xscores.com/exporter/?state=clientStructure&type=18'
        );
        $TimeZones = simplexml_load_string($TimeZones);
        foreach ($TimeZones->List->Item as $TimeZone)
        {
            TimeZone::create([
                'name' => $TimeZone['name']
            ]);
        }
        $matches = Match::all();
        $user = Auth::User();
        return view('welcome', compact('user'));
    }

    public function getInfo()
    {

        $matches = League::with('match')->get();

        return response()->json($matches);
    }

    public function logout(Request $request)
    {

        $request->session()->invalidate();

        if ($request->wantsJson()) {
            return response()->json([], 204);
        }

        return redirect('/log_in');
    }
}
