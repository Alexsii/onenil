<?php

namespace App\Http\Controllers\Admin;

use App\User;
use function Couchbase\defaultDecoder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function home()
    {
        $users = User::all();
        return view('Admin.action.show_user', compact('users'));
    }

    public function dropInfo(Request $request){

        $id = $request->get('id');
        $user_info = User::findOrFail($id);
        return $user_info;
    }
}
