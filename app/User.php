<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    const DEFAULT_REFER_LINK = 'http://onenil/refer_link/';
    const FOLDER_USER_IMAGE = '/public/image/user_images/';

    protected $fillable = [
        'name', 'email', 'money', 'password', 'last_name', 'refer_link', 'paypal_link', 'link_friends', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
