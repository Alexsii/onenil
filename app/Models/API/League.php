<?php

namespace App\Models\API;

use Illuminate\Database\Eloquent\Model;

class League extends Model
{

    protected $fillable = [
        'id_league', 'name'
    ];

    public function match()
    {
        return $this->hasMany('App\Models\API\Match', 'id_league', 'id');
    }
}
