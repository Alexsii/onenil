<?php

namespace App\Models\API;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $fillable = [
        'id_league', 'id_match', 'first_team_id', 'second_team_id'
    ];

    public function league()
    {

    }
}
