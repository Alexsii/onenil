<?php

namespace App\Models\API;

use Illuminate\Database\Eloquent\Model;

class SoccertTeam extends Model
{
    protected $fillable = [
        'id_team', 'image', 'name',
    ];
}
