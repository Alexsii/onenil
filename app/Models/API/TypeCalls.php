<?php

namespace App\Models\API;

use Illuminate\Database\Eloquent\Model;

class TypeCalls extends Model
{
    const REQUEST_UPDATE = 1;
    const REQUEST_DAY = 2;
    const REQUEST_MONTH = 3;
    const REQUEST_WEEK = 4;
    const REQUEST_CUSTOM = 5;
    const REQUEST_LEAGUES = 6;
    const REQUEST_TABLE = 7;
    const REQUEST_FINISHED = 8;
    const REQUEST_MATCH = 10;

    const REQUEST_SPORTS = 11;
    const REQUEST_SEASONS = 12;
    const REQUEST_COUNTRIES = 13;
    const REQUEST_COMPETITIONS = 14;
    const REQUEST_PHASES = 15;
    const REQUEST_ROUNDS = 16;
    const REQUEST_TEAMS = 17;
    const REQUEST_TIMEZONE = 18;
    const REQUEST_RANKINGS = 19;
    const REQUEST_TOURNAMENTS = 21;
}
