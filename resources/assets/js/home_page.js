window.$ = window.jQuery = require('jquery');
let checkDate = false;
let checkLeague = false;

$('.select_section #date').click(function () {
    if ($('.select_section #league').hasClass('open')){
        $('.select_section #league').removeClass('open');
        $('#league_list').removeClass('open_list');
        checkLeague = false;
    }
      if (checkDate != true){
         $('.select_section #date').addClass('open');
         $('#date_list').addClass('open_list');
         $('.select_list').addClass('select_list_open');
         checkDate = !checkDate;
      } else{
          $('.select_section #date').removeClass('open');
          $('#date_list').removeClass('open_list');
          $('.select_list').removeClass('select_list_open');
          checkDate = !checkDate;
      }
});
$('.select_section #league').click(function () {
    if ($('.select_section #date').hasClass('open')){
        $('.select_section #date').removeClass('open');
        $('#date_list').removeClass('open_list');
        checkDate = false;
    }
    if (checkLeague != true){
        $('.select_section #league').addClass('open');
        $('#league_list').addClass('open_list');
        $('.select_list').addClass('select_list_open');
        checkLeague = !checkLeague;
    } else{
        $('.select_section #league').removeClass('open');
        $('#league_list').removeClass('open_list');
        $('.select_list').removeClass('select_list_open');
        checkLeague = !checkLeague;
    }
});