require('./bootstrap');

window.Vue = require('vue');
window.VueRouter = require('vue-router');

import VueRouter from 'vue-router';

import Logo from './components/Logo';
import Logo_in from './components/Logo_in';
import FilterSelect from './components/FilterSelect';
import League from './components/League';
import OnePick from './components/pick_user/OnePick';
import Confirmed from './components/pick_user/Confirmed';
import News from './components/pick_user/Newsfeed';
import ReferalLink from './components/pick_user/ReferalLink';
import FriendsRefer from './components/pick_user/InviteReferal'
import MyAccount from './components/pick_user/UserAccount'
import Footeronenil from './components/FooterOneNil';

import NotFound from './components/PageNotFound';

Vue.use(VueRouter);

Vue.component('logo', require('./components/Logo'));
Vue.component('logo_in', require('./components/Logo_in'));
Vue.component('loginview', require('./components/LoginView'));
Vue.component('filterselect', require('./components/FilterSelect'));
Vue.component('league', require('./components/League'));
Vue.component('footeronenil', require('./components/FooterOneNil'));
Vue.component('onepick', require('./components/pick_user/OnePick'));
Vue.component('news', require('./components/pick_user/Newsfeed'));
Vue.component('confirmed', require('./components/pick_user/Confirmed'));
Vue.component('referallink', require('./components/pick_user/ReferalLink'));

/*<filterselect></filterselect>*/
const routes = [

    {
        path: '/log_in',
        name: 'log_in',
        component: Logo,
    },
    {
        path: '/catalog/',
        name: 'catalog',
        component: Logo_in,
        children: [
            {
                path: '',
                components: {
                    FilterSelect: FilterSelect,
                    League: League,
                },
            },
            {
                path: 'onepick',
                name: 'onepick',
                component: OnePick,
            },
            {
                path: 'confirmed',
                name: 'confirmed',
                component: Confirmed,
            },
            {
                path: 'referal',
                name: 'referal',
                component: ReferalLink,
            },
            {
                path: 'friend_referal',
                name: 'friend_referal',
                component: FriendsRefer,
            },
            {
                path: 'news',
                name: 'news',
                component: News
            },
            {
                path: 'myaccount',
                name: 'myaccount',
                component: MyAccount,
            },
        ]
    },
    {
        path: '/user/logout',
    },
    {
        path: '*',
        component: NotFound,
    }

];

const router = new VueRouter({
    mode: 'history',
    base: '/public/',
    routes,
});

new Vue({
    router,
}).$mount('#app');