let check = false;

$('.logo_menu').click(function () {
    if (!$('.logo_menu').hasClass('logo_menu_active')) {
        $('.logo_menu').addClass('logo_menu_active');
        $('.logo_menu .img').addClass('img_active');
        $('.logo_select').addClass('logo_select_active');
        check = !check;
    } else {
        $('.logo_menu').removeClass('logo_menu_active');
        $('.logo_menu .img').removeClass('img_active');
        $('.logo_select').removeClass('logo_select_active');
        check = !check;
    }
});
$('.logo_select').click(function () {
    $('.logo_menu').removeClass('logo_menu_active');
    $('.logo_menu .img').removeClass('img_active');
    $('.logo_select').removeClass('logo_select_active');
    check = !check;
});