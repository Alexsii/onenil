<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        window.laravel = {csrfToken: '{{ csrf_token() }}'}
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/home_page.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/one_pick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/confirmed.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/logo.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/create_refer.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/user_account.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/news.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/friend_referal.css') }}">
    <title>OneNil</title>

    <!-- Fonts -->
{{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}


<!-- Styles -->
</head>
<body style="margin: 0;">
<div id="app">
    <div class="header">
        <router-view></router-view>
    </div>
</div>
{{--        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                    @endauth
                </div>
            @endif
        </div>--}}
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/logo.js') }}"></script>
<script src="{{ asset('js/home_page.js') }}"></script>
</body>
</html>
