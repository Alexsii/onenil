@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('css')
    @yield('style_admin_list')
@endsection()

@section('content_header')
    @yield('title_page')
@stop

@section('content')
    @yield('admin_page')
@stop

@section('js')
    @yield('admin_js')
@endsection()