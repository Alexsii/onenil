@extends('Admin.home')

@section('style_admin_list')
    <link rel="stylesheet" href="{{ asset('css/admin_style.css') }}">
@endsection()

@section('title_page')
        <h1>User list</h1>
@endsection()


@section('admin_page')
        <div class="user_list">
            <div class="user_list_box">
                @foreach($users as $user)
                <div class="list_section" data-id="{{ $user->id }}">
                    <div>
                        <label for="popupCheckboxOne" class="popup-shower">
                            <a href="#">
                                {{ $user->name }}, {{ $user->last_name }}
                            </a>
                        </label>
                    </div>
                    <div>
                        <p><span>$</span>{{ $user->money }}</p>{{--капуня--}}
                        <p>1</p>{{--кол-во ставок--}}
                        <a href="admin/list/delete/{{$user->id}}">Delete</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @yield('drop_info')
        <div class="popup-wrapper">
            <input type="checkbox" class="popup-checkbox" id="popupCheckboxOne">
            <div class="popup">
                <div class="popup-content">
                    <label for="popupCheckboxOne" class="popup-closer">&#215;</label>
                            <div class="user_title" id="user_title">
                                <img src="{{ \App\User::FOLDER_USER_IMAGE.$user->image}}" alt="photo user">
                                <p>{{$user->name}}</p>
                            </div>
                            <div class="user_info">
                                <ul>
                                    <li id="email"></li>
                                    <li id="refer_link"></li>
                                    <li id="paypal_link"></li>
                                    <li id="link_friends"></li>
                                    <li id="money"></li>
                                </ul>
                            </div>
                            <div class="user_delete" data-del-user="">
                                Delete user
                            </div>
                </div>
            </div>
        </div>
@endsection()

@section('admin_js')
    <script src="{{ asset('js/admin.js') }}" type="text/javascript"></script>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">

    </script>

    <script>
 $('.list_section').click(function () {

     let id = $(this).data('id');
     $.ajax({
         type:'POST',
         // method:'POST',
         url:'/public/admin/post_data_user',
         data: {id: id, _token: '<?php echo csrf_token() ?>'},
         success:function(data){
             // $("#msg").html(data.msg);

             $('#email').text(data.email)
             $('#refer_link').text(data.refer_link)
             $('#paypal_link').text(data.paypal_link)
             $('#link_friends').text(data.link_friends)
             $('#money').text(data.money)
             $('#user_title p').text(data.name +''+ data.last_name)

         }
     });
        return false;
 });
    </script>
@endsection