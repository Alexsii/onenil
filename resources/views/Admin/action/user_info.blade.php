@extends('Admin.action.show_user')
@section('drop_info')
<div class="popup-wrapper">
    <input type="checkbox" class="popup-checkbox" id="popupCheckboxOne">
    <div class="popup">
        <div class="popup-content">
            <a href="/public/admin/list">
                <label class="popup-closer">&#215;</label>
            </a>
                    <div class="user_title">
                        <img src="{{ \App\User::FOLDER_USER_IMAGE.$user_info->image}}" alt="photo user">
                        <p>{{$user_info->name}}</p>
                    </div>
                    <div class="user_info">
                        <ul>
                            <li>{{$user_info->email}}</li>
                            <li>{{\App\User::DEFAULT_REFER_LINK.$user_info->refer_link}}</li>
                            <li>PayPal{{$user_info->paypal_link}}</li>
                            <li>{{$user_info->link_friends}}</li>
                            <li>${{$user_info->money}}</li>
                        </ul>
                    </div>
                    <div class="user_delete">
                        <a href="{{ url('admin/list/delete/' . $user_info->id ) }}">Delete user</a>
                    </div>
        </div>
    </div>
</div>
@endsection